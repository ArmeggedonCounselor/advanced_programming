﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {

    public Magic pickup;
    public bool replace = true;

    public void Start () {
    }

	public void OnTriggerEnter2D(Collider2D coll ) {
        pickup.OnPickup(replace, coll);
        Destroy(gameObject);
    }
}
