﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour {

    public Transform spellSource;
    public CastMagic castMagic;
    public LambdaExample lambdaExample;
    public List<Magic> spellList;

    public delegate void CastMagic ();
    public delegate float LambdaExample ( float x );

    public void Start () {
        lambdaExample = x => x * x;
        spellList = new List<Magic>();
    }
}
