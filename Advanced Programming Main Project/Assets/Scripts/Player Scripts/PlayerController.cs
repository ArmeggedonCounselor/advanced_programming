﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    private PlayerCombat pc;
    private Transform tf;
    public float walkSpeed;
    private float timeAwake = 0f;
	// Use this for initialization
	void Start () {
        pc = GetComponent<PlayerCombat>();
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        timeAwake += Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.Z)) {
            if (pc.castMagic != null)
            {
                pc.castMagic();
            }
        }
        if (Input.GetKeyDown(KeyCode.T)) {
            Debug.Log(pc.lambdaExample(timeAwake));
        }
        if (Input.GetKey(KeyCode.LeftArrow)) {
            tf.position -= tf.right * (walkSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.RightArrow)) {
            tf.position += tf.right * (walkSpeed * Time.deltaTime);
        }
    }
}
