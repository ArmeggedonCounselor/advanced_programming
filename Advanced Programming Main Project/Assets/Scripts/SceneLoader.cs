﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

    public static SceneLoader sl;

    void Awake () {
        if (sl != null) {
            Debug.Log("No more than one SceneLoader should be active.");
            Destroy(this.gameObject);
        }
        sl = this;
        DontDestroyOnLoad(this.gameObject);
    }

    public void Update () {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            Load("Menu");
        }
    }

    public void Load ( string scene ) {
        SceneManager.LoadScene(scene);
    }
}
