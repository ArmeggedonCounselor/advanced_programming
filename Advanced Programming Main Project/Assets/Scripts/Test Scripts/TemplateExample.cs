﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemplateExample<T>
{

    public List<T> list;

    public TemplateExample(List<T> newList)
    {
        list = newList;
    }
}
