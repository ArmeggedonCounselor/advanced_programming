﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public class MultithreadingExample : MonoBehaviour {

    public TemplateExample<InventoryItem> inventory;
    public TemplateExample<int> randomNumbers;

	// Use this for initialization
	void Start () {
        
	}

    public void makeLists()
    {
        PopulateLists();
        SetInventory();
    }

    void PopulateLists(){
        List<int> numberList = new List<int>();
        
        for(int i = 0; i < 20; i++)
        {
            numberList.Add(Random.Range(0, 100));
        }

        randomNumbers = new TemplateExample<int>(numberList);
    }

    void SetInventory()
    {
        List<InventoryItem> newInventory = new List<InventoryItem>();
        newInventory.Add(new InventoryItem("Sword", 2.0f, 5, 3.0f, "A plain steel sword."));
        newInventory.Add(new InventoryItem("Book", 0.0f, 10, 2.0f, "Does not contain secrets of the universe. Does contain a nice pie recipe."));
        newInventory.Add(new InventoryItem("Spear", 4.0f, 3, 3.0f, "Combine with a magic helmet to hunt rabbits."));
        newInventory.Add(new InventoryItem("Axe", 5.0f, 3, 10.0f, "For chopping wood, or people."));
        newInventory.Add(new InventoryItem("+50 Dagger of Horripilation", 1.0f, 100, 0.2f, "You're pretty sure this is just a normal dagger with glitter glued to the handle."));
        inventory = new TemplateExample<InventoryItem>(newInventory);
    }

    public void SortandList()
    {
        inventory.list.Sort(InventoryItem.sortInventoryList());
        randomNumbers.list.Sort();


        foreach (InventoryItem item in inventory.list)
        {
            Debug.Log(item.makeString());
        }
        foreach (int integer in randomNumbers.list)
        {
            Debug.Log(integer);
        }
    }

    public void spawnNewThread()
    {
        Thread t = new Thread(SortandList);
        t.Start();
        t.Join();

    }
}
