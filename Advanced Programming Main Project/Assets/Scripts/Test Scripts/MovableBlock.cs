﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableBlock : MonoBehaviour {

    private Transform tf;
    private Vector3 startPos;
    private Vector3 endPos;
    public Transform endTarget;
    public float speed;
    public float waitTime;

    public bool startMoving = false;

    private float totalDistance;
    private float timeElapsed = 0f;

    // Use this for initialization
    void Start () {
        tf = GetComponent<Transform>();
        startPos = tf.position;
        endPos = endTarget.position;
        totalDistance = Vector3.Distance(startPos, endPos);
        if (startMoving) {
            StartCoroutine("MoveBlock");
        }
    }

    public void StartMovement () {
        StartCoroutine("MoveBlock");
    }

    // This coroutine will move this block between the starting position and the end position through linear interpolation.
    public IEnumerator MoveBlock () {
        // Since Lerp needs a percentage of the distance between two objects to determine where to place the new position,
        // we need to keep track of the total distance traveled.
        float distanceTraveled = 0f;
        float distancePercent = 0f;

        // Normally, infinite loops like this are a bigtime no-no, especially to call every frame draw.
        // However, we can make this loop a lot safer by having specified break points.
        while (true) {
            // Within the loop, we want to make a few logic checks.
            // First, we check to see if we are currently at the endPosition.
            if (tf.position != endPos) { // If we are....

                // This is just simple algebra. velocity = distance/time, so multiplying speed by time will give us the distance traveled.
                // On the first loop, this is zero, by design.
                distanceTraveled = speed * timeElapsed;
                // Again, this is simple math. The part over the whole equals the percentage.
                distancePercent = distanceTraveled / totalDistance;
                // Now we set our current position to the position that represents that percentage of the way between the starting position and the end position.
                tf.position = Vector3.Lerp(startPos, endPos, distancePercent);
                // And just before we yield, we go ahead and increment timeElapsed by the time it took to draw the last frame.
                timeElapsed += Time.deltaTime;
                // This ensures that we will come back to this coroutine when the next frame is drawn.
                yield return null;
            }
            else { // ... and if we're not.
                // A simple swap. (Note to self, come back to this when showing off the Template.)
                Vector3 temp;
                temp = startPos;
                startPos = endPos;
                endPos = temp;

                // Once these are set, we have to reset our variables.
                distanceTraveled = 0f;
                distancePercent = 0f;
                timeElapsed = 0f;

                // And now, we tell the coroutine to wait.
                // This means that the coroutine will not continue until "waitTime" seconds have elapsed.
                // This is a good way to create timing based platforming and puzzles.
                yield return new WaitForSeconds(waitTime);
            }
        }
    }
}
