﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryItem : IComparer<InventoryItem>
{

    string _name;
    float _power;
    int _value;
    float _weight;
    string _description;

    public SortFlag flag = SortFlag.NAME;

    public enum SortFlag { NAME, POWER, VALUE, WEIGHT };

    public InventoryItem()
    {
        _name = "";
        _power = 0.0f;
        _value = 0;
        _weight = 0.0f;
        _description = "";
    }

    public InventoryItem(string name, float power, int value, float weight, string description)
    {
        _name = name;
        _power = power;
        _value = value;
        _weight = weight;
        _description = description;
    }

    public void ChangeSortFlag(SortFlag newSort)
    {
        flag = newSort;
    }

    public int Compare(InventoryItem x, InventoryItem y)
    {
        switch (x.flag)
        {
            case SortFlag.NAME:
                return string.Compare(x.GetName(), y.GetName());
            case SortFlag.POWER:
                if(x.GetPower() > y.GetPower()){ return 1; }
                else if (x.GetPower() < y.GetPower()){ return -1; }
                else { return 0; }
            case SortFlag.VALUE:
                return x.GetValue() - y.GetValue();
            case SortFlag.WEIGHT:
                if(x.GetWeight() > y.GetWeight()) { return 1; }
                else if (x.GetWeight() < y.GetWeight()) { return -1; }
                else { return 0; }
            default:
                return 0;
        }
    }

    public string makeString()
    {
        return _name + " - " + _power.ToString() + " - " + _value.ToString() + " - " + _weight.ToString() + " - \"" + _description + "\" \n";
    }

    public string GetName() { return _name; }

    public float GetPower() { return _power; }

    public int GetValue() { return _value; }

    public float GetWeight() { return _weight; }

    public static IComparer<InventoryItem> sortInventoryList()
    {
        return (IComparer<InventoryItem>)new InventoryItem();
    }
}
