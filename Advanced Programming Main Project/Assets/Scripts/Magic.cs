﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Magic : MonoBehaviour {

    public float damage;
    public float speed;
    public float lifeTime;
    public GameObject visuals;
    protected GameObject spell;

    protected PlayerCombat pc;

    public abstract void OnPickup ( bool replace, Collider2D coll);

    public abstract void Cast ();

    public abstract IEnumerator Travel (GameObject spell);
}
