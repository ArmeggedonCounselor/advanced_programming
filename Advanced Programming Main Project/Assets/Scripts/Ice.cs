﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ice : Magic {

    public override void OnPickup ( bool replace, Collider2D coll ) {
        pc = coll.gameObject.GetComponent<PlayerCombat>();
        if (pc != null) {
            if (replace) {
                pc.castMagic = this.Cast;
                pc.spellList.Clear();
                pc.spellList.Add(this);
            }
            else {
                pc.castMagic += this.Cast;
                pc.spellList.Add(this);
            }
        }
    }

    public override void Cast () {
        StartCoroutine("Travel", Instantiate(visuals, pc.spellSource));
    }

    public override IEnumerator Travel (GameObject spell) {
        float elapsedTime = 0;
        while (true) {
            if (elapsedTime < lifeTime) {
                spell.transform.position += spell.transform.right * (speed * Time.deltaTime);
                elapsedTime += Time.deltaTime;
                yield return null;
            }
            else {
                Destroy(spell);
                yield break;
            }
        }
    }
}
